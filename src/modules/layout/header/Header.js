// import { Link } from "react-router-dom";
import Button from "react-bootstrap/Button";
import Container from "react-bootstrap/Container";
import Form from "react-bootstrap/Form";
import Nav from "react-bootstrap/Nav";
import Navbar from "react-bootstrap/Navbar";
import { useAuth } from "../../auth/components/Auth";

function Header() {
  // useEffect(() =>{
  //   const getUser = () =>{
  //     axios.get("https://dummyjson.com/auth/me",{
  //       headers: {
  //         "Authorization": `Bearer ${localStorage.getItem("token")}`
  //       }
  //     }).then((response)=>{
  //       console.log(response)
  //     })
  //   }
  //   getUser();
  // })

  const { saveAuth } = useAuth();
  return (
    <Navbar expand="lg" className="bg-body-tertiary">
      <Container fluid>
        <Navbar.Brand href="#">KKPOB</Navbar.Brand>
        <Navbar.Toggle aria-controls="navbarScroll" />
        <Navbar.Collapse id="navbarScroll">
          <Nav
            className="me-auto my-2 my-lg-0"
            style={{ maxHeight: "100px" }}
            navbarScroll
          >
            <Nav.Link href="/">Home</Nav.Link>
            <Nav.Link href="/blog">Blog</Nav.Link>

            <Nav.Link href="/contactus">Contact</Nav.Link>
            {/* <Button onClick={()=>saveAuth(null)} variant="outline-success">Logout</Button> */}
          </Nav>
          <Form className="d-flex">
            <Form.Control
              type="search"
              placeholder="Search"
              className="me-2"
              aria-label="Search"
            />
            <Button variant="outline-success">Search</Button>
          </Form>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
}

export default Header;
