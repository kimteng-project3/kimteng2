import axios from "axios"
import { getAuth } from "../../auth/components/AuthHelper";

const getUsers = () =>{
    return axios.get("/api/users",{
        headers: {
            Authorization: `Bearer ${getAuth()}`
        }
    });
}

export{getUsers}