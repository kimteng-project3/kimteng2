import Header from "./header/Header";
import { Outlet } from "react-router-dom";
import MainLayout from "./pages/mainLayout";

function Layout() {
  return (
    <>
      
      <MainLayout />
    </>
  );
}

export default Layout;
