import axios from "axios";
import { getAuth } from "../components/AuthHelper";

const login = (username, pass) => {
  return axios.post(
    "/api/auth/login",
    {
      username: username,
      password: pass,
    },
    {
      headers: {
        Authorization: "Basic aG90ZWw6aG90ZWxAMTIz",
      },
    }
  );
};

const getUser = () => {
  return axios.get("/auth/me", {
    headers: {
      "Authorization": `Bearer ${getAuth()}`,
    },
  });
};

export { login, getUser };
