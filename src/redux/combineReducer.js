import {combineReducers} from "redux";
import usersSlice from "../modules/users/core/reducer";

export const rootReducers = combineReducers({
    users: usersSlice
})