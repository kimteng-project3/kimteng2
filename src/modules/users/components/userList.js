import React from "react";
import { Button, Space, Table } from "antd";
import { useEffect } from "react";
import { useUsers } from "../core/action";
import { useSelector } from "react-redux";
import { MdEditSquare } from "react-icons/md";
import { AiFillDelete } from "react-icons/ai";

const UserList = () => {
  const { fetchUsers } = useUsers();
  const { list } = useSelector((state) => state.users);

  useEffect(() => {
    fetchUsers();
  }, []);

  console.log(list);

  const { Column } = Table;
  return (
    <Table dataSource={list}>
      {" "}
      //jab data tam dataIndex
      <Column title="Name" dataIndex="name" key="name" />
      <Column title="Address" dataIndex="address" key="address" />
      <Column title="Email" dataIndex="email" key="email" />
      <Column title="Phone" dataIndex="phone" key="phone" />
      <Column title="Bio" dataIndex="bio" key="bio" />
      <Column
        title="Action"
        key="action"
        render={() => (
          <Space size="middle">
            <a>
              {" "}
              <Button>
                <MdEditSquare />
                Edit
              </Button>
            </a>
            <a>
              <Button>
                <AiFillDelete />
                Delete
              </Button>
            </a>
          </Space>
        )}
      />
    </Table>
  );
};

export default UserList;

{
  /*<Column*/
}
{
  /*  title="Tags"*/
}
{
  /*  dataIndex="tags"*/
}
{
  /*  key="tags"*/
}
{
  /*    />*/
}
{
  /*  {list.map((lists) => (*/
}
{
  /*        <Tag color="blue" key={lists.name}>*/
}
{
  /*          {lists.name}*/
}
{
  /*        </Tag>*/
}
{
  /*      ))}*/
}

// const data = [
//   {
//     key: '1',
//     firstName: 'John',
//     lastName: 'Brown',
//     age: 32,
//     address: 'New York No. 1 Lake Park',
//     tags: ['nice', 'developer'],
//   },
//   {
//     key: '2',
//     firstName: 'Jim',
//     lastName: 'Green',
//     age: 42,
//     address: 'London No. 1 Lake Park',
//     tags: ['loser'],
//   },
//   {
//     key: '3',
//     firstName: 'Joe',
//     lastName: 'Black',
//     age: 32,
//     address: 'Sydney No. 1 Lake Park',
//     tags: ['cool', 'teacher'],
//   },
// ];
